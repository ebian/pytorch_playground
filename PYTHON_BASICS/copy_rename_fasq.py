import shutil
import os
folder_src = 'fastqdata'
src = os.path.join(os.getcwd(),folder_src )
folder_ds = 'fastqdata1'
ds = os.path.join(os.getcwd(),folder_ds )

dic_b = {
    "k":"kboy",
    "m":"mboy"
}

dir_files_src = os.listdir(src)

for file_src in dir_files_src:
    file_ = file_src[:-6]
    if file_ in dic_b.keys():
        n_w_ext = dic_b[file_]
        new_src = os.path.join(src,file_src)
        new_des = os.path.join(ds,n_w_ext + ".fastq")
        shutil.copy(new_src,new_des)



'''
   -folder1--data
   -folder2--nodata
    code.py


'''